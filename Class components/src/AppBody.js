import React, { Component } from "react";

import axios from "axios";

import SearchBar from "./SearchBar"
import UsersList from "./UsersList";
import UserDetails from "./UserDetails"
import SearchResult from "./SearchResult";

class AppBody extends Component
{

  state = {
    userToShowDetailsId: 0,
    imagesSearchResult: []
  }

  // Cette fonction permet de changer l'id de l'user sélectionné.
  // Dans le render(), on passe cette fonction au component enfant "UsersList" en la faisant passer par les props.
  changeUserToShow = ( userId ) =>
  {
    this.setState( { userToShowDetailsId: userId } );
  }

  searchImageSubmit = async ( keywords ) =>
  {
    const response = await axios.get(
      "https://api.unsplash.com/search/photos",
      {
        params: { query: keywords },
        headers: { Authorization: "Client-ID 9a98423479840bb4358b922d0d16966e9dba11ec7dcc1778d4306a723cca42f9" }
      }
    );
    this.setState( { imagesSearchResult: response.data.results } );
    console.log( this.state.imagesSearchResult );
  }

  render ()
  {
    return (
      <div className="container">
        <SearchBar searchImageSubmit={ this.searchImageSubmit } />
        <SearchResult results={ this.state.imagesSearchResult } />
        <UsersList callbackChangeUserToShow={ this.changeUserToShow } />
        <UserDetails userId={ this.state.userToShowDetailsId } />
      </div>
    );
  }
}

export default AppBody;