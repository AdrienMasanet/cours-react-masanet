import React from "react";

const AppFooter = () => {
  return (
    <section className="w-100 bg-secondary mt-4">
      <div style={ {minHeight: "100px"} }></div>
      <div className="w-100 h-25 mt-auto text-center text-light p-2" style={ {background: "rgba(0,0,0,.1)"} }>copyright mazaland</div>
    </section>
  );
}

export default AppFooter;