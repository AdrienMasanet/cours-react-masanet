import React from "react";

const AppHeader = () =>
{
  return (
    <section className="w-100 h-25 mt-auto text-center mb-4 p-4" style={ {background: "rgba(0,0,0,.1)"} }>
      <h1>Exercice React</h1>
    </section>
  );
}

export default AppHeader;