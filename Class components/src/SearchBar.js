import React, { Component } from "react";

export default class SearchBar extends Component
{
  state = {
    keywords: ""
  }

  onFormSubmit = ( event ) =>
  {
    event.preventDefault();
    this.props.searchImageSubmit(this.state.keywords);
  }

  render ()
  {
    return (
      <div className="container">
        <form onSubmit={ this.onFormSubmit }>
          <div className="form-group">
            <label className="form-label" htmlFor="term">Recherche d'image</label>
            <input className="form-control" onChange={ event => this.setState( { keywords: event.target.value } ) } type="text" id="keywords" val={ this.state.keywords }></input>
          </div>
        </form>
      </div>
    );
  }
}