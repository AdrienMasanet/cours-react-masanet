import React from "react";

const SearchResult = ( props ) =>
{
  return (
    <div>
      <p>Résultats de la recherche :</p>
      <div className="d-flex flex-wrap justify-content-start">
        { props.results.map( ( res ) =>
        {
          return (
            <img className="m-2" style={ { width: "205px", height: "fit-content" } } src={ res.urls.regular } />
          );
        } ) }
      </div>
    </div>
  );
}

export default SearchResult;