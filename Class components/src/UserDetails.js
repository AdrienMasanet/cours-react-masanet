import React from "react";

import { userMock } from "./models/UserMock";

const UserDetails = ( props ) =>
{

  const user = userMock[ props.userId ];

  return (
    <div className="d-flex justify-content-center">
      <div className="card m-2 d-inline" style={ { width: "300px" } }>
        <img className="card-img-top" src={ user.avatar } alt="Card cap" />
        <div className="card-body">
          <h5 className="card-title">{ user.firstName }</h5>
          <p className="card-text">{ user.firstName } { user.lastName }, { user.age } ans.</p>
        </div>
      </div>
    </div>
  );
}

export default UserDetails;