import React, { Component } from "react";
import { userMock } from "./models/UserMock";

class UsersList extends Component
{
  state = {
    list: []
  }  

  // Cette fonction est une référence à ChangeUserToShow provenant du component parent "appBody", elle est
  // transmise de "appBody" à ce composant par les props de ce dernier, l'appeller appelle donc la fonction
  // du component parent.
  callbackChangeUserToShow = ( userId ) =>
  {
    this.props.callbackChangeUserToShow( userId );
  }

  componentDidMount ()
  {
    this.setState( { list: userMock } )
  }

  render ()
  {
    return (
      <div className="d-flex flex-wrap justify-content-start">
        {this.state.list.map( ( user, index ) =>
        {
          return (
            <div key={ index } onClick={ () => { this.callbackChangeUserToShow( index ) } } className="card m-2 btn" style={ { width: "205px" } }>
              <img className="card-img-top" src={ user.avatar } alt="Card image cap" />
              <div className="card-body">
                <h5 className="card-title">{ user.firstName }</h5>
                <p className="card-text">{ user.firstName } { user.lastName }<br /> { user.age } ans.</p>
              </div>
            </div>
          );
        } ) }
      </div>
    );
  }

}

export default UsersList;