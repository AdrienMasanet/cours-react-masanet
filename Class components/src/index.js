import React, { Fragment } from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import AppHeader from "./AppHeader"
import AppBody from "./AppBody"
import AppFooter from "./AppFooter"

const App = () => {
  return (
    <Fragment>
      <AppHeader />
      <AppBody />
      <AppFooter />
    </Fragment>
  );
}

ReactDOM.render( <App />, document.querySelector( "#root" ) );