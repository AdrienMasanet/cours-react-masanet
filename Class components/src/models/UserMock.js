import Faker from "faker";

const users = [];

for(let i=0 ; i<=6 ; i++) {

  let user={
    firstName: Faker.name.firstName(),
    lastName: Faker.name.lastName(),
    age: Faker.datatype.number({min: 10, max: 100}),
    avatar: Faker.image.avatar()
  }

  users.push(user);
}

export const userMock = users;