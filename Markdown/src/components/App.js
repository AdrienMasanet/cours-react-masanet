import React, { Component } from "react";
import "./app.css";

import marked from "marked";

export default class App extends Component
{
  state = {
    text: ""
  }

  componentDidMount ()
  {
    const text = localStorage.getItem( "text" );
    if ( text ) {
      this.setState( { text } );
    } else {
      this.setState( { text: "pas encore de texte" } );
    }
  }

  componentDidUpdate ()
  {
    // C'est bizarre mais c'est la bonne façon de récupérer une (ou des) variable(s) dans le state du component
    const { text } = this.state;
    localStorage.setItem("text", text);
  }

  handleChange = event =>
  {
    const text = event.target.value;
    this.setState( { text } );
  }

  renderText = text =>
  {
    const __html = marked(text, { sanitize: true });
    return { __html };
  }

  render ()
  {
    return (
      <div className="container m-5">
        <div className="row">
          <div className="col-sm-6">
            <textarea
              onChange={ this.handleChange }
              rows="22"
              className="form-control"
              value={ this.state.text }>
            </textarea>
          </div>
          <div className="col-sm-6">
            <div dangerouslySetInnerHTML={ this.renderText( this.state.text ) }></div>
          </div>
        </div>
      </div >
    )
  }
}