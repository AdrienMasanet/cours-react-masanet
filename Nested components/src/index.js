import React from "react";
import ReactDOM from "react-dom";

import faker from "faker";

import CommentDetail from "./CommentDetail";
import ApprovedCommentDetail from "./ApprovedCommentDetail";

const App = () =>
{
  return (
    <div className="ui container comments">
      <ApprovedCommentDetail>
        <CommentDetail author={ faker.name.firstName() } date="Today" text={ faker.lorem.sentence() } />
      </ApprovedCommentDetail>
      <CommentDetail author={ faker.name.firstName() } date="Today" text={ faker.lorem.sentence() } />
      <CommentDetail author={ faker.name.firstName() } date="Today" text={ faker.lorem.sentence() } />
      <CommentDetail author={ faker.name.firstName() } date="Today" text={ faker.lorem.sentence() } />
      <CommentDetail author={ faker.name.firstName() } date="Today" text={ faker.lorem.sentence() } />
      <CommentDetail author={ faker.name.firstName() } date="Today" text={ faker.lorem.sentence() } />
      <CommentDetail author={ faker.name.firstName() } date="Today" text={ faker.lorem.sentence() } />
      <CommentDetail author={ faker.name.firstName() } date="Today" text={ faker.lorem.sentence() } />
      <CommentDetail author={ faker.name.firstName() } date="Today" text={ faker.lorem.sentence() } />
    </div>
  );
}

ReactDOM.render( <App />, document.querySelector( "#root" ) );